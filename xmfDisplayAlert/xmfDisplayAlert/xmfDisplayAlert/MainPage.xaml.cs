﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace xmfDisplayAlert
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            buOk.Clicked += BuOk_Clicked;
            buYesNo.Clicked += BuYesNo_Clicked;
            buSelect.Clicked += BuSelect_Clicked;
            buInputStr.Clicked += BuInputStr_Clicked;
            buInputInt.Clicked += BuInputInt_Clicked;
        }

        private async void BuInputInt_Clicked(object sender, EventArgs e)
        {
            String str = await DisplayPromptAsync("Запрос", "Напиши любое число", keyboard: Keyboard.Numeric);
            await DisplayAlert("Твой ответ", str, "Ok");
        }

        private async void BuInputStr_Clicked(object sender, EventArgs e)
        {
            String str = await DisplayPromptAsync("Запрос", "Как тебя зовут?");
            await DisplayAlert("Твой ответ", str, "Ok");
        }

        private async void BuSelect_Clicked(object sender, EventArgs e)
        {

            String str = await DisplayActionSheet("Выбор", "Отмена", null, "Москва", "Воронеж", "Тула");
            await DisplayAlert("Твой ответ", str, "Ok");
        }

        private async void BuYesNo_Clicked(object sender, EventArgs e)
        {
            bool r = await DisplayAlert("Вопрос", "Сделай выбор", "Да", "Нет");
            await DisplayAlert("Твой выбор", r.ToString(), "Ok");
        }

        private async void BuOk_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Предупреждение", "Это текст предупреждения", "Ok");
        }
    }
}
