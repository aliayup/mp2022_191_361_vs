﻿using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace Audioteka
{
    class Music
    {
        public Music(  string filename, string date_time, byte[] musicData)
        {
            Filename = filename;
            Date_time = date_time;
            MusicData = musicData;
        }
        public string Filename { get; private set; }
        public string Date_time { get; private set; }
        public byte[] MusicData { get; private set; }
    }
}