using Npgsql;
using System;
using System.Collections;
using System.Data;
using System.Runtime.InteropServices;

namespace Audioteka
{
    public partial class Audio : Form
    {
        private string contactstring = String.Format("Server={0};Port={1};" +
            "User Id={2};Password={3};Database={4};",
            "localhost", 5432, "postgres",
            "17052016", "dbDemo");
        private NpgsqlConnection contact;
        private string sql,sql1,sql_count;
        private NpgsqlCommand cmd,cmd1,cmd_count;
        private DataTable dt;
        string[] paths, files;
        string fileName;
        WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();

        public Audio()
        {
            BackColor = Color.Black;
            //ForeColor = Color.White;
            //progressBar.ForeColor = Color.FromArgb(150,0,0);
            //progressBar.BackColor = Color.FromArgb(150, 0, 0);

            InitializeComponent();
            tb_volume.Value = 50;
        }
        private void Select()
        {
            try
            {
                contact.Open();
                sql = @"select name from audio";
                cmd = new NpgsqlCommand(sql, contact);
                dt = new DataTable();
                NpgsqlDataReader SDR = cmd.ExecuteReader();
                musicList.Items.Clear();
                bool MoreResults=false;
                do
                {
                    while (SDR.Read())
                    {
                        for (int i = 0; i < SDR.FieldCount; i++)
                        {
                            musicList.Items.Add(SDR[i]);
                        }
                    }
                    MoreResults = SDR.NextResult();
                    }
                    while (MoreResults) ;
                SDR.Close();
                contact.Close();
                //for (int x = 0; x < files.Length; x++)
                //{
                //    musicList.Items.Add(files[x]);
                //}
            }
            catch (Exception ex)
            {
                contact.Close();
                MessageBox.Show("Error: " + ex.Message);
            }
        }
        private void SelectFile()
        {
            try
            {
                contact.Open();
                sql = @"select musicData from audio";
                sql1 =@"select name from audio";
                
                dt = new DataTable();
                cmd = new NpgsqlCommand(sql, contact);
                cmd1 = new NpgsqlCommand(sql1, contact);
                ArrayList arrayList = new ArrayList();
                
                NpgsqlDataReader SDR = cmd.ExecuteReader();
                int j = 0;
                while (SDR.Read()) {
                        j++;

                        byte[] buffer = (byte[])SDR[0];
                        System.IO.File.WriteAllBytes(j.ToString()+".wav", buffer);
                }
                SDR.Close();
                contact.Close();
                //for (int x = 0; x < files.Length; x++)
                //{
                //    musicList.Items.Add(files[x]);
                //}
            }
            catch (Exception ex)
            {
                contact.Close();
                MessageBox.Show("������ �� �����: " + ex.Message);
            }
        }
        private void Audio_Load(object sender, EventArgs e)
        {
            contact=new NpgsqlConnection(contactstring);
            value_volume.Text = tb_volume.Value.ToString() + "%";
            Select();
            SelectFile();
            if (musicList.Items.Count > 0)
            {
                musicList.SelectedIndex=0;

                player.controls.stop();
            }
        }

        private void play_btn_Click(object sender, EventArgs e)
        {
            player.controls.play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void next_btn_Click(object sender, EventArgs e)
        {
            if (musicList.SelectedIndex < musicList.Items.Count - 1)
            {
                musicList.SelectedIndex = musicList.SelectedIndex + 1;
            }
        }

        private void last_bnt_Click(object sender, EventArgs e)
        {
            if (musicList.SelectedIndex > 0)
            {
                musicList.SelectedIndex = musicList.SelectedIndex - 1;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (player.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                progressBar.Maximum = (int)player.controls.currentItem.duration;
                progressBar.Value = (int)player.controls.currentPosition;
            }
            
            try
            {
                
                if(musicList.SelectedItems.Count!=0)
                    label_end.Text = player.controls.currentItem.durationString.ToString();
                label_start.Text=player.controls.currentPositionString;
            }
            catch
            {

            }
        }

        private void tb_volume_Scroll(object sender, EventArgs e)
        {
            player.settings.volume = tb_volume.Value;
            value_volume.Text = tb_volume.Value.ToString()+"%";
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void progressBar_MouseDown(object sender, MouseEventArgs e)
        {
            player.controls.currentPosition = player.currentMedia.duration * e.X / progressBar.Width;
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void value_volume_Click(object sender, EventArgs e)
        {

        }

        private void last_bnt_MouseEnter(object sender, EventArgs e)
        {
            last_bnt.BackColor = Color.White;
            last_bnt.ForeColor = Color.Black;
        }

        private void last_bnt_MouseLeave(object sender, EventArgs e)
        {
            last_bnt.BackColor = Color.Black;
            last_bnt.ForeColor = Color.White;
        }

        private void play_btn_MouseEnter(object sender, EventArgs e)
        {

            play_btn.BackColor = Color.White;
            play_btn.ForeColor = Color.Black;
        
        }

        private void play_btn_MouseLeave(object sender, EventArgs e)
        {
            play_btn.BackColor = Color.Black;
            play_btn.ForeColor = Color.White;
        }

        private void pause_btn_MouseEnter(object sender, EventArgs e)
        {
            pause_btn.BackColor = Color.White;
            pause_btn.ForeColor = Color.Black;
        }

        private void pause_btn_MouseLeave(object sender, EventArgs e)
        {
            pause_btn.BackColor = Color.Black;
            pause_btn.ForeColor = Color.White;
        }

        private void next_btn_MouseEnter(object sender, EventArgs e)
        {
            next_btn.BackColor = Color.White;
            next_btn.ForeColor = Color.Black;
        }

        private void next_btn_MouseLeave(object sender, EventArgs e)
        {
            next_btn.BackColor = Color.Black;
            next_btn.ForeColor = Color.White;
        }

        private void stop_btn_MouseEnter(object sender, EventArgs e)
        {
            stop_btn.BackColor = Color.White;
            stop_btn.ForeColor = Color.Black;
        }

        private void stop_btn_MouseLeave(object sender, EventArgs e)
        {
            stop_btn.BackColor = Color.Black;
            stop_btn.ForeColor = Color.White;
        }

        private void open_btn_MouseEnter(object sender, EventArgs e)
        {
            open_btn.BackColor = Color.LightCoral;
            open_btn.ForeColor = Color.White;
        }

        private void open_btn_MouseLeave(object sender, EventArgs e)
        {
            open_btn.BackColor = Color.White;
            open_btn.ForeColor = Color.LightCoral;
        }

        private void open_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                files = ofd.FileNames;
                paths = ofd.FileNames;
                byte[] musicData;
                int id;
                
                for (int i = 0; i < files.Length; i++)
                {
                    files[i]=Path.GetFileNameWithoutExtension(ofd.FileNames[i]);
                    
                    // ��������� �����
                    using (System.IO.FileStream fs = new System.IO.FileStream(paths[i], FileMode.Open))
                    {
                        musicData = new byte[fs.Length];
                        fs.Read(musicData, 0, musicData.Length);
                    }
                    try
                    {
                        contact.Open();
                        sql = @"insert into audio values(:date_time,:name,:musicData)";
                        cmd = new NpgsqlCommand(sql, contact);

                        cmd.Parameters.AddWithValue("date_time", DateTime.Now);
                        cmd.Parameters.AddWithValue("name",files[i]);
                        cmd.Parameters.AddWithValue("musicData", musicData);
                        dt.Load(cmd.ExecuteReader());
                        contact.Close();
                        Select();
                        SelectFile();
                    }
                    catch (Exception ex)
                    {
                        contact.Close();
                        MessageBox.Show(ex.Message);
                    }
                }
                
            }
        }

        private void pause_Click(object sender, EventArgs e)
        {
            player.controls.pause();
        }

        private void musicList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int f = musicList.SelectedIndex+1;
            player.URL = @"C:\\Users\\Lenovo\\source\\repos\\mp2022_191_361_vs\\Audioteka\\Audioteka\\bin\\Debug\\net6.0-windows\\"+f.ToString()+".wav";

            
            player.controls.play();
        }

        private void stop_btn_Click(object sender, EventArgs e)
        {
            player.controls.stop();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}