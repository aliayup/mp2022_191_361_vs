﻿namespace Audioteka
{
    partial class Audio
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.last_bnt = new System.Windows.Forms.Button();
            this.next_btn = new System.Windows.Forms.Button();
            this.play_btn = new System.Windows.Forms.Button();
            this.pause_btn = new System.Windows.Forms.Button();
            this.stop_btn = new System.Windows.Forms.Button();
            this.open_btn = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.musicList = new System.Windows.Forms.ListBox();
            this.tb_volume = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label_start = new System.Windows.Forms.Label();
            this.label_end = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.v = new System.Windows.Forms.Label();
            this.value_volume = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tb_volume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // last_bnt
            // 
            this.last_bnt.BackColor = System.Drawing.Color.Black;
            this.last_bnt.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.last_bnt.ForeColor = System.Drawing.Color.White;
            this.last_bnt.Location = new System.Drawing.Point(24, 384);
            this.last_bnt.Name = "last_bnt";
            this.last_bnt.Size = new System.Drawing.Size(47, 54);
            this.last_bnt.TabIndex = 1;
            this.last_bnt.Text = "🠔";
            this.last_bnt.UseVisualStyleBackColor = false;
            this.last_bnt.Click += new System.EventHandler(this.last_bnt_Click);
            this.last_bnt.MouseEnter += new System.EventHandler(this.last_bnt_MouseEnter);
            this.last_bnt.MouseLeave += new System.EventHandler(this.last_bnt_MouseLeave);
            // 
            // next_btn
            // 
            this.next_btn.BackColor = System.Drawing.Color.Black;
            this.next_btn.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.next_btn.ForeColor = System.Drawing.Color.White;
            this.next_btn.Location = new System.Drawing.Point(239, 384);
            this.next_btn.Name = "next_btn";
            this.next_btn.Size = new System.Drawing.Size(48, 54);
            this.next_btn.TabIndex = 2;
            this.next_btn.Text = "🠖";
            this.next_btn.UseVisualStyleBackColor = false;
            this.next_btn.Click += new System.EventHandler(this.next_btn_Click);
            this.next_btn.MouseEnter += new System.EventHandler(this.next_btn_MouseEnter);
            this.next_btn.MouseLeave += new System.EventHandler(this.next_btn_MouseLeave);
            // 
            // play_btn
            // 
            this.play_btn.BackColor = System.Drawing.Color.Black;
            this.play_btn.Font = new System.Drawing.Font("Arial Black", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.play_btn.ForeColor = System.Drawing.Color.White;
            this.play_btn.Location = new System.Drawing.Point(77, 384);
            this.play_btn.Name = "play_btn";
            this.play_btn.Size = new System.Drawing.Size(74, 54);
            this.play_btn.TabIndex = 3;
            this.play_btn.Text = "🞂";
            this.play_btn.UseVisualStyleBackColor = false;
            this.play_btn.Click += new System.EventHandler(this.play_btn_Click);
            this.play_btn.MouseEnter += new System.EventHandler(this.play_btn_MouseEnter);
            this.play_btn.MouseLeave += new System.EventHandler(this.play_btn_MouseLeave);
            // 
            // pause_btn
            // 
            this.pause_btn.BackColor = System.Drawing.Color.Black;
            this.pause_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.pause_btn.ForeColor = System.Drawing.Color.White;
            this.pause_btn.Location = new System.Drawing.Point(157, 384);
            this.pause_btn.Name = "pause_btn";
            this.pause_btn.Size = new System.Drawing.Size(76, 54);
            this.pause_btn.TabIndex = 4;
            this.pause_btn.Text = "▌ ▌";
            this.pause_btn.UseVisualStyleBackColor = false;
            this.pause_btn.Click += new System.EventHandler(this.pause_Click);
            this.pause_btn.MouseEnter += new System.EventHandler(this.pause_btn_MouseEnter);
            this.pause_btn.MouseLeave += new System.EventHandler(this.pause_btn_MouseLeave);
            // 
            // stop_btn
            // 
            this.stop_btn.BackColor = System.Drawing.Color.Black;
            this.stop_btn.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.stop_btn.ForeColor = System.Drawing.Color.White;
            this.stop_btn.Location = new System.Drawing.Point(293, 384);
            this.stop_btn.Name = "stop_btn";
            this.stop_btn.Size = new System.Drawing.Size(65, 54);
            this.stop_btn.TabIndex = 5;
            this.stop_btn.Text = "■";
            this.stop_btn.UseVisualStyleBackColor = false;
            this.stop_btn.Click += new System.EventHandler(this.stop_btn_Click);
            this.stop_btn.MouseEnter += new System.EventHandler(this.stop_btn_MouseEnter);
            this.stop_btn.MouseLeave += new System.EventHandler(this.stop_btn_MouseLeave);
            // 
            // open_btn
            // 
            this.open_btn.BackColor = System.Drawing.Color.White;
            this.open_btn.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.open_btn.ForeColor = System.Drawing.Color.LightCoral;
            this.open_btn.Location = new System.Drawing.Point(364, 384);
            this.open_btn.Name = "open_btn";
            this.open_btn.Size = new System.Drawing.Size(205, 54);
            this.open_btn.TabIndex = 6;
            this.open_btn.Text = "Открыть";
            this.open_btn.UseVisualStyleBackColor = false;
            this.open_btn.Click += new System.EventHandler(this.open_btn_Click);
            this.open_btn.MouseEnter += new System.EventHandler(this.open_btn_MouseEnter);
            this.open_btn.MouseLeave += new System.EventHandler(this.open_btn_MouseLeave);
            // 
            // progressBar
            // 
            this.progressBar.ForeColor = System.Drawing.SystemColors.Desktop;
            this.progressBar.Location = new System.Drawing.Point(24, 345);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(754, 22);
            this.progressBar.TabIndex = 7;
            this.progressBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.progressBar_MouseDown);
            // 
            // musicList
            // 
            this.musicList.BackColor = System.Drawing.Color.White;
            this.musicList.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.musicList.ForeColor = System.Drawing.Color.LightCoral;
            this.musicList.FormattingEnabled = true;
            this.musicList.ItemHeight = 26;
            this.musicList.Location = new System.Drawing.Point(293, 12);
            this.musicList.Name = "musicList";
            this.musicList.Size = new System.Drawing.Size(485, 290);
            this.musicList.TabIndex = 8;
            this.musicList.SelectedIndexChanged += new System.EventHandler(this.musicList_SelectedIndexChanged);
            // 
            // tb_volume
            // 
            this.tb_volume.Location = new System.Drawing.Point(572, 384);
            this.tb_volume.Maximum = 100;
            this.tb_volume.Name = "tb_volume";
            this.tb_volume.Size = new System.Drawing.Size(181, 56);
            this.tb_volume.TabIndex = 9;
            this.tb_volume.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tb_volume.Scroll += new System.EventHandler(this.tb_volume_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(598, 413);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 28);
            this.label1.TabIndex = 10;
            this.label1.Text = "Громкость";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_start
            // 
            this.label_start.AutoSize = true;
            this.label_start.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_start.ForeColor = System.Drawing.Color.White;
            this.label_start.Location = new System.Drawing.Point(24, 321);
            this.label_start.Name = "label_start";
            this.label_start.Size = new System.Drawing.Size(51, 21);
            this.label_start.TabIndex = 11;
            this.label_start.Text = "00:00";
            // 
            // label_end
            // 
            this.label_end.AutoSize = true;
            this.label_end.Font = new System.Drawing.Font("Tw Cen MT Condensed Extra Bold", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_end.ForeColor = System.Drawing.Color.LightCoral;
            this.label_end.Location = new System.Drawing.Point(741, 321);
            this.label_end.Name = "label_end";
            this.label_end.Size = new System.Drawing.Size(47, 21);
            this.label_end.TabIndex = 12;
            this.label_end.Text = "00:00";
            this.label_end.Click += new System.EventHandler(this.label2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // v
            // 
            this.v.AutoSize = true;
            this.v.Location = new System.Drawing.Point(718, 72);
            this.v.Name = "v";
            this.v.Size = new System.Drawing.Size(0, 20);
            this.v.TabIndex = 13;
            this.v.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // value_volume
            // 
            this.value_volume.AutoSize = true;
            this.value_volume.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.value_volume.ForeColor = System.Drawing.Color.White;
            this.value_volume.Location = new System.Drawing.Point(743, 413);
            this.value_volume.Name = "value_volume";
            this.value_volume.Size = new System.Drawing.Size(45, 28);
            this.value_volume.TabIndex = 14;
            this.value_volume.Text = "0%";
            this.value_volume.Click += new System.EventHandler(this.value_volume_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Audioteka.Properties.Resources.img_2270;
            this.pictureBox1.Location = new System.Drawing.Point(33, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(244, 224);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // Audio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.value_volume);
            this.Controls.Add(this.v);
            this.Controls.Add(this.label_end);
            this.Controls.Add(this.label_start);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_volume);
            this.Controls.Add(this.musicList);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.open_btn);
            this.Controls.Add(this.stop_btn);
            this.Controls.Add(this.pause_btn);
            this.Controls.Add(this.play_btn);
            this.Controls.Add(this.next_btn);
            this.Controls.Add(this.last_bnt);
            this.Name = "Audio";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Audio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tb_volume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button last_bnt;
        private Button next_btn;
        private Button play_btn;
        private Button pause_btn;
        private Button stop_btn;
        private Button open_btn;
        private ProgressBar progressBar;
        private ListBox musicList;
        private TrackBar tb_volume;
        private Label label1;
        private Label label_start;
        private Label label_end;
        private ColorDialog colorDialog1;
        private System.Windows.Forms.Timer timer1;
        private Label v;
        private Label value_volume;
        private PictureBox pictureBox1;
    }
}