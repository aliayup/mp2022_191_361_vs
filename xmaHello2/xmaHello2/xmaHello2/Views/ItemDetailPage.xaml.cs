﻿using System.ComponentModel;
using Xamarin.Forms;
using xmaHello2.ViewModels;

namespace xmaHello2.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}