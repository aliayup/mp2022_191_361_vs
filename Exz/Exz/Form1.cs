using System.Resources;

namespace Exz
{
    public partial class Form1 : Form
    {
        Button[] B;
        

        public Form1()
        {
            InitializeComponent();
            B = new Button[16];
            B[0] = button1;
            B[1] = button2;
            B[2] = button3;
            B[3] = button4;
            B[4] = button5;
            B[5] = button6;
            B[6] = button7;
            B[7] = button8;
            B[8] = button9;
            B[9] = button10;
            B[10] = button11;
            B[11] = button12;
            B[12] = button13;
            B[13] = button14; 
            B[14] = button15;
            B[15] = button16;
            panel1_Resize(null,null);
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            for (int i = 0; i < 16; i++)
            {
                int x, y;
                y = i / 4;
                x = i - y * 4;
                B[i].Top = y * panel1.Height / 4;
                B[i].Left = x * panel1.Width / 4;
                B[i].Width =panel1.Width / 4;
                B[i].Height =panel1.Height / 4;
            }
        }
        private void Move()
        {

        }
    }
}