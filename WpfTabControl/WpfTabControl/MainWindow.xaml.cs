﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTabControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void cmdAddPage_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var stack = new StackPanel() { Orientation = Orientation.Horizontal };
            stack.Children.Add(new TextBlock() { Text = "Button" });
            stack.Children.Add(new Button()
            {
                Content = "x",
                Command = MyCommands.cmdDeleteCurPage,
                Margin = new Thickness() { Left = 5, Right = 5 },
                Padding = new Thickness() { Left = 5, Right = 5 }
            });
            
            tc.Items.Add(new TabItem { Header = stack });
        }

        private void cmdDeleteCurPage_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            
            tc.Items.Remove(tc.SelectedItem);
        }
    }
    public class MyCommands
    {
        public static RoutedCommand cmdAddPage { get; set; } = new RoutedCommand("cmdAddPage", typeof(MainWindow));

        public static RoutedCommand cmdDeleteCurPage { get; set; } = new RoutedCommand("cmdDeleteCurPage", typeof(MainWindow));
    }
}

