﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace xmfPagesList
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            List<Page> pages = new List<Page>();
            pages.Add(new UserPage());
            pages.Add(new UserPage());
            pages.Add(new UserPage());
            pages.Add(new UserPage());
            pages.Add(new UserPage());
            main.ItemsSource = pages;
        }

        private async void Main_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await this.Navigation.PushAsync((Page)e.SelectedItem);
            }
            main.SelectedItem = null;
        }
    }
}   