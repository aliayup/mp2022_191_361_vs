﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Tetris
{
    /// <summary>
    /// Логика взаимодействия для Records.xaml
    /// </summary>
    public partial class Records : Window
    {
        public Records()
        {
            InitializeComponent();
            setScores("record.txt", true);
        }
        private async void Back_Click(object sender, RoutedEventArgs e)
        {
         
            this.Hide();

        }
        private void setScores(string filename, bool isLeft)
        {
            string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), $"{filename}.txt");
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                for (int i = 0; i < lines.Length; i++)
                {
                    Label label = new Label();
                    label.Content = $"{i + 1}-{lines[i]}";
                    label.VerticalAlignment = VerticalAlignment.Top;
                    Style style = this.TryFindResource("userScoreLabel") as Style;
                    label.Style = style;
                    if (isLeft)
                    {
                        label.HorizontalAlignment = HorizontalAlignment.Left;
                        label.Margin = new Thickness(60, 120 + i * 20, 0, 0);
                    }
                    else
                    {
                        label.HorizontalAlignment = HorizontalAlignment.Right;
                        label.Margin = new Thickness(0, 120 + i * 20, 60, 0);
                    }
                    this.main.Children.Add(label);
                    
                }
            }
        }
    }
}
