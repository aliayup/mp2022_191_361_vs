﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public class BlockQuere
    {
        private readonly Block[] blocks = new Block[]
       {
            new IBlock(),
            new JBlock(),
            new LBlock(),
            new OBlock(),
            new SBlock(),
            new TBlock(),
            new ZBlock()
       };
        private readonly Random random=new Random();
        //следующий блок
        public Block NextBlock { get; private set; }
        //метод возвращающиц случайный блок
        public Block RandomBlock()
        {
            return blocks[random.Next(blocks.Length)];
        }
        // следующий блок рандомим
        public BlockQuere()
        {
            NextBlock = RandomBlock();
        }
        // возвращает следуюющий блок 
        public Block GetAndUpdate()
        {
            Block block = NextBlock;
            do
            {
                NextBlock = RandomBlock();
            }
            //продолжаем выбирать пока не получим новый блок
            while (block.Id == NextBlock.Id);
            return block;
        }
    }
}
