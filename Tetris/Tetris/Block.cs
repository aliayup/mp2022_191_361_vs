﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public abstract class Block
    {
        protected abstract Position[][] Tiles { get; }
        protected abstract Position StartOffset { get; }
        public abstract int Id { get; }

        private int rotationState;
        private Position offset;

        public Block()
        {
            offset = new Position(StartOffset.Row, StartOffset.Column);
        }
        //позиции сетки, занимаемые блоком с учетом текущего поворота и смещения
        public IEnumerable<Position> TilesPositions()
        {
            foreach (Position p in Tiles[rotationState])
            {
                yield return new Position(p.Row + offset.Row, p.Column + offset.Column);
            }
        }
        // поворот по часовой стрелки на 90 градусов
        public void RotateCW()
        {
            // увеличиваем текущение состояние вращения и оборачиваем в 0 если находится в конечном состоянии
            rotationState=(rotationState+1)%Tiles.Length;
        }
        // поворот против часовой стрелки
        public void RotateCCW()
        {
            if (rotationState == 0)
            {
                rotationState = Tiles.Length - 1;
            }
            else
            {
                rotationState--;
            }
        }
        //перемещает блок с заданным количеством строк и колонок
        public void Move(int rows,int columns)
        {
            offset.Row += rows;
            offset.Column += columns;
        }
        // метод которые сбрасывает текущее положение блок и повороты
        public void Reset()
        {
            rotationState = 0;
            offset.Row = StartOffset.Row;
            offset.Column = StartOffset.Column;
        }
    }
}
