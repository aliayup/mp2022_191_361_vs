﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public class GameGrid
    {
        private readonly int[,] grid;
        public int Rows { get; }
        public int Columns { get; }
        // индексатор
        public int this[int r,int c]
        {
            get => grid[r, c];
            set => grid[r, c] = value;
        }
        public GameGrid(int rows,int columns)
        {
            Rows = rows;
            Columns = columns;
            grid = new int[rows, columns];
        }
        // проверка заданный столбец и строка находится ли в сетке
        public bool IsInside(int r,int c)
        {
            return r>=0 && r<Rows && c>=0 && c<Columns;
        }
        // проверка пуста ли данная ячейка
        public bool IsEmpty(int r,int c)
        {
            return IsInside(r, c) && grid[r, c] == 0;
        }
        //заполнена ли строка
        public bool isRowFull(int r)
        {
            for (int c = 0; c < Columns; c++)
            {
                if (grid[r, c] == 0)
                {
                    return false;
                }
            }
            return true;
        }
        //проверка пустая ли строка
        public bool isRowEmpty(int r)
        {
            for (int c = 0; c < Columns; c++)
            {
                if (grid[r, c] != 0)
                {
                    return false;
                }
            }
            return true;
        }
        // очистка строки
        public void ClearRow(int r)
        {
            for (int c = 0; c < Columns; c++)
            {
                grid[r, c] = 0;

            }
        }
        //перемещение на определенное количество строк 
        private void MoveRowDown(int r,int numbRows)
        {
            for (int c = 0; c < Columns; c++)
            {
                grid[r + numbRows, c] = grid[r, c];
                grid[r, c] = 0; 
            }
        }
        public int ClearFullRows()
        {
            int cleared = 0;
            // двигаемся от нижней строки к верхней
            for (int r = Rows-1; r >= 0; r--)
            {
                //заполнена ли текущая строка
                if (isRowFull(r))
                {
                    //очищаем строку и увеличиваем число на которое нужно переместить на одну
                    ClearRow(r);
                    cleared++;
                }
                //если очищено больше 0 перемещаем текущую строку вниз на количество очищенных строк
                else if(cleared > 0)
                {
                    MoveRowDown(r, cleared);
                }
            }
            // возвращаем количество очищенных строк
            return cleared;
        }
    }
}
